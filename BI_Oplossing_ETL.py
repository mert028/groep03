import mysql.connector
import pandas as pd
import numpy as np
import requests
import json
import re
import datetime
import tweepy
from bs4 import BeautifulSoup
from pytrends.request import TrendReq
from sqlalchemy import create_engine
import pymysql



year_start = datetime.datetime.now().year
month_start = datetime.datetime.now().month
day_start = datetime.datetime.now().day
date = [str(year_start) + "-" +  str(month_start) + "-" + str(day_start)]

# test comment CHANGED

# Mert heeft een comment toegevoegd
def connectToDB():   
    host        = "127.0.0.1"
    user        = "root"
    passw       = "IPBDAMgroep3"
    port        = 3306 
    database    = 'TDM_Final'

    mydb = create_engine('mysql+pymysql://' + user + ':' + passw + '@' + host + ':' + str(port) + '/' + database , echo=False)

    if(mydb):
        print("Connectie gelukt!")
        print(mydb)
        return mydb
    else:
        print("Connectie niet gelukt!")
        return False

# APi van Binance pakt data van Litecoin
def pakBinanceData(googleTrend):
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)

    url = 'https://api.binance.com/api/v3/ticker/24hr'
    binance_df = pd.DataFrame(requests.get(url).json())
    # print(binance_df)
    gegevens_ltc = binance_df[binance_df["symbol"] == "LTCUSDT"]
    
    to_delete = [
        "symbol", "askPrice", "priceChange", "priceChangePercent",
        "weightedAvgPrice", "lastQty", "openTime", "closeTime", "askQty",
        "bidQty", "firstId", "lastId", "bidPrice", "quoteVolume"
        ]

    for col in to_delete:
        del gegevens_ltc[col]
        
    openPrice = gegevens_ltc['openPrice']
    highPrice = gegevens_ltc['highPrice']
    lowPrice = gegevens_ltc['lowPrice']
    price = gegevens_ltc['lastPrice']
    closePrice = gegevens_ltc['prevClosePrice']
    volume = gegevens_ltc['volume']
    count = gegevens_ltc['count']

    new_data = {'prijs':price,
                'hoogste_prijs':highPrice,
                'laagste_prijs':lowPrice,
                'open_prijs':openPrice,
                'closed_prijs':closePrice,
                'volume':volume,
                'aantal_coins':count,
                'googleTrendWaarde':googleTrend,
                'datum':date}
    binance_dataFrame = pd.DataFrame(new_data)

    return binance_dataFrame


# WEB SCRAPPING -- beurs: AEX
def pakAEXkoers():
    url = "https://www.debeurs.nl/Koersen/Europa_Lokale_Beurzen/Amsterdam/AEX.aspx"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, features="html.parser")
    for a in soup.findAll('td', attrs={'class': 'ValueCell'}):
        AEXkoers = a.find('span', attrs={'id': '12272LastPrice'}) 
        if AEXkoers != None:
            AEXkoers = str(AEXkoers).split('>')[1].split('<')[0]
            AEXkoers = AEXkoers.replace("," , ".")
            AEXkoers = float(AEXkoers)
            new_data = {'naam_beurs':['AEX'], 'prijs':[AEXkoers], 'datum':date}
            AEXkoers_dataFrame = pd.DataFrame(new_data)
            return AEXkoers_dataFrame

# APi van Twitter 
# gebruikt module Tweepy
def pakTwitterBerichten():
    consumer_key        = 'sZ4fil5JWewDHy53gLftckub4'
    consumer_secret     = 'OawgmfKhs5mWi5oBpz6cfrOVHr6OfQfW5FgRZaO510pA4VQN8M'
    access_token        = '1243497219069825025-a9rb6wREGorilhLTtmlFdDHXGaDZLI'
    access_token_secret = 'HYML9SIFebMtkuINcnTqmEenMd9yQvUKmIJF9L0gW8Iqg'
        
    hashtag_phrase = 'litecoin'
    
    #create authentication for accessing Twitter
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    #initialize Tweepy API
    api = tweepy.API(auth)
    
    #get the name of the spreadsheet we will write to
    fname = '_'.join(re.findall(r"#(\w+)", hashtag_phrase))
    
    #open the spreadsheet we will write to
    tweets_dataFrame = pd.DataFrame()

    for tweet in tweepy.Cursor(api.search, q=hashtag_phrase+' -filter:retweets', lang="en",  tweet_mode='extended').items(20):
        
        date1 = [tweet.created_at][0].date()
        
        time = [tweet.created_at][0].time()
        followers_count = tweet.user.followers_count
        twitter_name = str(tweet.user.screen_name)
        twitter_message = str(tweet.full_text)
        
        new_data = {'bericht':twitter_message,
                    'username':twitter_name,
                    'followers_count':followers_count,
                    'datum':date1,
                    'tijd':time}
        tweets_dataFrame = tweets_dataFrame.append(new_data, ignore_index=True)
    
    return tweets_dataFrame

# WEB SCRAPPING -- GOUD PER KILO IN EURO
def pakGoudKoers():
    url = "https://www.goudstandaard.com/koersgrafiek"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, features="html.parser")
    for a in soup.findAll('table', attrs={'id': 'actual_rates'}):
        GoudKoers = str(a.find('td', attrs={'class': 'data'})).split('€')[1].split(' ')[1].split('<')[0]
        new_data = {'GoudKoers':[GoudKoers]}
        Goud_dataFrame = pd.DataFrame(new_data)
        GoudKoers = GoudKoers.replace("," , ".")
        GoudKoers = GoudKoers.split(".")
        GoudKoers = GoudKoers[0] + GoudKoers[1] + "."  + GoudKoers[2]
        GoudKoers = float(GoudKoers)
        new_data = {'naam_beurs':['Goud'], 'prijs':[GoudKoers], 'datum':date}
        GoudKoers_dataFrame = pd.DataFrame(new_data)
        return GoudKoers_dataFrame

# WEB SCRAPPING -- APPLE in Amerikaanse dollars.
def pakAppleKoers():
    url = "https://www.debeurs.nl/Koersen/Verenigde_Staten.aspx"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, features="html.parser")
    for a in soup.findAll('td', attrs={'id': 'AAPL.BATS,ELastPriceCell'}):
        AppleKoers = str(a.find('span', attrs={'id': 'AAPL.BATS,ELastPrice'})).split('>')[1].split('<')[0]
        AppleKoers = AppleKoers.replace(",",".")
        new_data = {'naam_beurs':['Apple'], 'prijs':[AppleKoers], 'datum':date}
        Apple_dataFrame = pd.DataFrame(new_data)
        return Apple_dataFrame

# APi Google Trends --  Pak interesse van zoekresultaten 'Litecoin' 
# Maakt gebruik van module: Pytrends
def pakGoogleTrendsData():
    pytrends = TrendReq(hl='en-US', tz=360)
    
    kw_list = ["Litecoin"]
    pytrends.build_payload(kw_list, cat=0, timeframe='now 1-H', geo='', gprop='')
    
    now = datetime.datetime.now()
    
    # pakt om 12 uur middag van de datum van vandaag de interesse van de zoekresultaten van Google Trends
    # De waarde is tussen 0 en 100, hoe hoger de waarde, hoe hoger de interesse. 
    waardeInteresse = pytrends.get_historical_interest(kw_list, year_start=now.year, month_start=now.month, day_start=now.day, hour_start=0, year_end=now.year, month_end=now.month, day_end=now.day, hour_end=0, cat=0, geo='', gprop='', sleep=0)
    
    waardeInteresse = waardeInteresse.to_numpy()[0][0]
        
    if waardeInteresse != None:
        return waardeInteresse
    else:
        return 'NONE'
    
def main():
    mydb = connectToDB()
    
    if(mydb):
        #Beurzen
        AEXKoers_DF = pakAEXkoers()
        GoudKoers_DF = pakGoudKoers()
        AppleKoers_DF = pakAppleKoers()

        #Litecoin
        googleTrend = pakGoogleTrendsData()
        Litecoin_DF = pakBinanceData(googleTrend)

        #Twitter
        Twitter_DF = pakTwitterBerichten()
        
        #schrijf naar table beurs
        AEXKoers_DF.to_sql(name='beurs', con=mydb, if_exists = 'append', index=False)
        GoudKoers_DF.to_sql(name='beurs', con=mydb, if_exists = 'append', index=False)
        AppleKoers_DF.to_sql(name='beurs', con=mydb, if_exists = 'append', index=False)
        Litecoin_DF.to_sql(name='coin', con=mydb, if_exists = 'append', index=False)
        Twitter_DF.to_sql(name='nieuws', con=mydb, if_exists = 'append', index=False)

        print("Dateframes succesvol ingevoerd in de database!")
        mydb.dispose()
        print("Connectie gesloten!")
        
if __name__ == "__main__":
    main()



