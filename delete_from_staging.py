import mysql.connector
import pandas as pd
import numpy as np
import requests
import json
import re
import datetime
import tweepy
from bs4 import BeautifulSoup
from pytrends.request import TrendReq
from sqlalchemy import create_engine
import pymysql
import datetime

# maak connectie met Operationele Database
def connectToDB(keuze_DB):   
    host        = "127.0.0.1"
    user        = "root"
    passw       = "IPBDAMgroep3"
    port        = 3306 
    database    = keuze_DB

    mydb = create_engine('mysql+pymysql://' + user + ':' + passw + '@' + host + ':' + str(port) + '/' + database , echo=False)

    if(mydb):
        print("Connectie gelukt!")
        print(mydb)
        return mydb
    else:
        print("Connectie niet gelukt!")
        return False

def main():
    # maak connectie met staging area
    mydb = connectToDB("staging_area")
    if(mydb):
        print("Connectie wordt gemaakt met staging area!")

        #verwijder alle oude data in staging area
        mydb.execute('DELETE from advies')
        mydb.execute('DELETE from beurs')
        mydb.execute('DELETE from coin')
     
        print("Staging opgeschoond!")
        mydb.dispose()
        print("Connectie gesloten!")

if __name__ == "__main__":
    main()



